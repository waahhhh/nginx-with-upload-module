#!/usr/bin/env sh
set -e

chown nginx /var/www/html/upload

exec "$@"
